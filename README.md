#Zadanie rekrutacyjne

Strona internetowa pisana na potrzeby rekrutacji.

Podczas tworzenia strony wykorzystano Google Web Starter Kit [https://developers.google.com/web/tools/starter-kit/](https://developers.google.com/web/tools/starter-kit/) .
W celu pracy na plikach z wykorzystaniem zawartych tam narzędzi należy postąpić zgodnie z instrukcją [https://developers.google.com/web/fundamentals/getting-started/web-starter-kit/](https://developers.google.com/web/fundamentals/getting-started/web-starter-kit/)

W celu przejrzenia plików roboczych oraz strony, pliki dostępne są do pobrania także jako archiwum .rar pod adresem [https://www.dropbox.com/s/2e126gt3l9fbmv9/Marcin_Kapica_zadanie_front_end_developer.rar?dl=0](https://www.dropbox.com/s/2e126gt3l9fbmv9/Marcin_Kapica_zadanie_front_end_developer.rar?dl=0)
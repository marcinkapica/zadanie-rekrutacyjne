$(document).ready(function() {
    initCarousel();
    affixDistance();
    smoothScroll();
    toggleTextBox();
});


function initCarousel () {
    $('.owl-carousel').owlCarousel({
        center: true,
        items:1,
        loop:true,
        dots:true,
        dotsContainer: '.js-dotsContainer',
        autoplay:true,
        autoplayTimeout:6000
    });
}

function affixDistance () {
    $('.js-navbar').affix({
        offset: {
            top: $(".header").outerHeight(true)
        }
    });
}


function smoothScroll () {
    jQuery('a[href*="#"]:not([href="#"])').click(function () {
        if (location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') && location.hostname === this.hostname) {
            var target = jQuery(this.hash);
            target = target.length ? target : jQuery('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                jQuery('html, body').animate({
                    scrollTop: target.offset().top - $("#navbar").outerHeight(false)
                }, 1000);
                return false;
            }
        }
    });
}

function toggleTextBox () {
    $(".js-textboxTab").click(function(){
        var box_id = $(this).attr("data-box");

        $(".js-textboxTab").removeClass("textboxes__tab--active");
        $(".js-textboxContent").removeClass("textboxes__box--active");

        $(this).addClass("textboxes__tab--active");
        $("#"+box_id).addClass("textboxes__box--active");
    })
}